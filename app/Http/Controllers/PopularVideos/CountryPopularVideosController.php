<?php

namespace App\Http\Controllers\PopularVideos;

use App\Interfaces\VideoInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Modules\CountryHitsModule\AllowedCountries;
use App\Modules\VideosModule\UseCases\GetLatestVideosByCountry;
use App\Modules\InformationModule\UseCases\GetInformationByCountry;

class CountryPopularVideosController extends Controller
{
    public function index()
    {
        $perPage = request()->input('offset') ?? 3;
        $allCountries = collect(AllowedCountries::all());

        $paginatedCountries = $allCountries->forPage(request()->input('page'), $perPage);

        $results = [];

        foreach ($paginatedCountries as $countryCode => $countryName) {
            try {
                $results[$countryCode] = $this->getPopularVideo($countryCode, $countryName);
            } catch (\Exception $e) {
                return response()->json($e->getMessage(), 500);
            }
        }

        return new LengthAwarePaginator($results, count($allCountries), $perPage);
    }

    public function show($countryCode)
    {
        $allowedCountries = AllowedCountries::all();

        if (isset($allowedCountries[$countryCode])) {
            return $this->getPopularVideo($countryCode, $allowedCountries[$countryCode]);
        }

        return response()->json(['country not allowed', 403]);
    }

    private function getPopularVideo($countryCode, $countryName)
    {
        $results = [];

        $results['info'] = $this->getInfo($countryName);
        $results['videos'] = $this->getVideos($countryCode);

        return $results;
    }

    private function getVideos($countryKey): array
    {
        return Cache::remember("country.popular.videos.$countryKey", (10 * 60), function () use ($countryKey) {
            $videos = [];
            $popularVideos = (new GetLatestVideosByCountry($countryKey))->execute();

            /** @var VideoInterface $video */
            foreach ($popularVideos as $video) {
                $videos[] = [
                    'title' => $video->title(),
                    'description' => $video->description(),
                    'code' => $video->code(),
                    'published_at' => $video->publishedAt(),
                    'thumbnail' => $video->thumbnail(),
                    'thumbnail_hq' => $video->thumbnailHQ(),
                ];
            }

            return $videos;
        });
    }

    private function getInfo($country): array
    {
        return Cache::remember("country.info.$country", (10 * 60), function () use ($country) {
            $info = [];

            $countryInfo = (new GetInformationByCountry($country))->execute();

            $info['title'] = $countryInfo->title();
            $info['summary'] = $countryInfo->firstParagraph();

            return $info;
        });
    }
}
