<?php

namespace App\Interfaces;

interface InformationInterface
{
    public function title(): string;

    public function firstParagraph(): string;
}
