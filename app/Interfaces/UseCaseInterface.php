<?php

namespace App\Interfaces;

interface UseCaseInterface
{
    public function execute();
}
