<?php

namespace App\Interfaces;

interface VideoInterface
{
    public function title(): string;

    public function description(): string;

    public function duration(): string;

    public function code(): string;

    public function publishedAt(): string;

    public function thumbnail(): string;

    public function thumbnailHQ(): string;
}
