<?php

namespace App\Modules\InformationModule\InformationProvider;

use App\Exceptions\InformationCanNotBeFound;
use App\Interfaces\InformationInterface;
use App\Modules\InformationModule\InformationEntity;

abstract class InformationProvider
{
    protected string $country;

    public function byCountry(string $country): InformationInterface
    {
        $this->country = $country;

        $pageData = $this->getByTitle();
        return $this->mapToEntities($pageData);
    }

    abstract protected function getByTitle(): array;

    abstract protected function mapToEntities(array $data): InformationInterface;
}
