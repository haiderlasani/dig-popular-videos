<?php

namespace App\Modules\InformationModule\InformationProvider;

use App\Interfaces\InformationInterface;
use App\Exceptions\InformationCanNotBeFound;
use App\Modules\InformationModule\InformationEntity;

class WikipediaInformationProvider extends InformationProvider
{
    protected function getByTitle(): array
    {
        try {
            $json = json_decode(file_get_contents(
                'https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&explaintext=1&titles=' . rawurlencode($this->country)),
                true
            );

            $pageData = collect($json['query']['pages'])->first();

            if (!isset($pageData['extract'])) {
                throw new InformationCanNotBeFound('Information can not be found');
            }

            return $pageData;
        } catch (\Exception $e) {
            throw new InformationCanNotBeFound($e->getMessage());
        }
    }

    protected function mapToEntities(array $data): InformationInterface
    {
        $title = $data['title'];
        $extract = $data['extract'];

        $informationEntity = new InformationEntity();

        $informationEntity->setTitle($title);
        $informationEntity->setFirstParagraph(
            $this->getFirstParagraph($extract)
        );

        return $informationEntity;
    }

    private function getFirstParagraph(string $text)
    {
        return substr($text, 0, strpos($text, '== '));
    }
}
