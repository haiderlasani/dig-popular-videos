<?php

namespace App\Modules\InformationModule;

use App\Interfaces\InformationInterface;

class InformationEntity implements InformationInterface
{
    private string $title;
    private string $firstParagraph;

    public function setTitle($value): void
    {
        $this->title = $value;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function setFirstParagraph($value): void
    {
        $this->firstParagraph = $value;
    }

    public function firstParagraph(): string
    {
        return $this->firstParagraph;
    }
}
