<?php

namespace App\Modules\InformationModule\UseCases;

use App\Interfaces\UseCaseInterface;
use App\Interfaces\InformationInterface;
use App\Exceptions\InformationCanNotBeFound;
use App\Modules\InformationModule\UseCaseImplementation\InformationByCountryImplementation;

class GetInformationByCountry extends InformationByCountryImplementation implements UseCaseInterface
{
    public function __construct(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return InformationInterface
     * @throws InformationCanNotBeFound
     */
    public function execute(): InformationInterface
    {
        return $this->fromWikipedia();
    }
}
