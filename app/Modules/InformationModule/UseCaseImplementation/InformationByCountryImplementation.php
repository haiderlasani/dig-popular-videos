<?php

namespace App\Modules\InformationModule\UseCaseImplementation;

use App\Interfaces\InformationInterface;
use App\Exceptions\InformationCanNotBeFound;
use App\Modules\InformationModule\InformationProvider\WikipediaInformationProvider;

class InformationByCountryImplementation
{
    protected string $country;

    /**
     * @return InformationInterface
     * @throws InformationCanNotBeFound
     */
    public function fromWikipedia(): InformationInterface
    {
        return (new WikipediaInformationProvider())->byCountry($this->country);
    }
}
