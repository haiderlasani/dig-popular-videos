<?php

namespace App\Modules\CountryHitsModule;

class AllowedCountries
{
    private static array $countries = [
        'nl' => 'Netherlands',
        'de' => 'Germany',
        'fr' => 'France',
        'es' => 'Spain',
        'it' => 'Italy',
        'gr' => 'Greece',
        'uk' => 'United Kingdom',
    ];

    public static function all()
    {
        return self::$countries;
    }
}
