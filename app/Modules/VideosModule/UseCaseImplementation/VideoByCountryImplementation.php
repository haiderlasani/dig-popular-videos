<?php

namespace App\Modules\VideosModule\UseCaseImplementation;

use App\Exceptions\VideosCanNotBeFound;
use App\Modules\VideosModule\VideoProviders\YoutubeVideoProvider;

class VideoByCountryImplementation
{
    protected string $country;

    /**
     * @return array
     * @throws VideosCanNotBeFound
     */
    public function fromYoutube(): array
    {
        $videoProvider = new YoutubeVideoProvider();
        $this->country = ($this->country == 'uk') ? 'gb' : $this->country;
        return $videoProvider->videosByCountry($this->country);
    }
}
