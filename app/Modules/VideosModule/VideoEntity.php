<?php

namespace App\Modules\VideosModule;

use App\Interfaces\VideoInterface;

class VideoEntity implements VideoInterface
{
    protected string $title;
    protected string $description;
    protected string $duration;
    protected string $code;
    protected string $publishedAt;
    protected string $thumbnail;
    protected string $thumbnailHQ;

    public function title(): string
    {
        return $this->title;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function duration(): string
    {
        return $this->duration;
    }

    public function code(): string
    {
        return $this->code;
    }

    public function publishedAt(): string
    {
        return $this->publishedAt;
    }

    public function setTitle($value)
    {
        $this->title = $value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    public function setDuration($value)
    {
        $this->duration = $value;
    }

    public function setCode($value)
    {
        $this->code = $value;
    }

    public function setPublishedAt($value)
    {
        $this->publishedAt = $value;
    }

    public function setThumbnailHQ($value)
    {
        $this->thumbnailHQ = $value;
    }

    public function setThumbnail($value)
    {
        $this->thumbnail = $value;
    }

    public function thumbnail(): string
    {
        return $this->thumbnail;
    }

    public function thumbnailHQ(): string
    {
        return $this->thumbnailHQ;
    }
}
