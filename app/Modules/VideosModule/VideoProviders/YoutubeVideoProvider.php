<?php

namespace App\Modules\VideosModule\VideoProviders;

use App\Interfaces\VideoInterface;
use Alaouy\Youtube\Facades\Youtube;
use App\Exceptions\VideosCanNotBeFound;
use App\Modules\VideosModule\VideoEntity;

class YoutubeVideoProvider extends VideoProvider
{
    /**
     * @return array
     * @throws VideosCanNotBeFound
     */
    protected function getVideosByCountry(): array
    {
        try {
            return Youtube::getPopularVideos(
                $this->country,
                5, // limit
                ['id', 'snippet', 'contentDetails', 'status']
            );
        } catch (\Exception $e) {
            // Catch exception for simplicity throwing an exception again
            throw new VideosCanNotBeFound('Videos can not be found', 400);
        }
    }

    /**
     * @param array $youtubeData
     * @return VideoInterface[]
     */
    protected function mapToEntities(array $youtubeData): array
    {
        $videos = [];
        foreach ($youtubeData as $youtubeVideo) {
            $videoEntity = new VideoEntity();

            $videoEntity->setTitle($youtubeVideo->snippet->title);
            $videoEntity->setDescription($youtubeVideo->snippet->description);
            $videoEntity->setDuration($youtubeVideo->contentDetails->duration);
            $videoEntity->setCode($youtubeVideo->id);
            $videoEntity->setPublishedAt($youtubeVideo->snippet->publishedAt);
            $videoEntity->setThumbnail($youtubeVideo->snippet->thumbnails->default->url);
            $videoEntity->setThumbnailHQ(
                $this->getHighestQualityThumbnail(
                    $youtubeVideo->snippet->thumbnails
                )
            );

            $videos[] = $videoEntity;
        }

        return $videos;
    }

    private function getHighestQualityThumbnail(object $thumbnailObject): string
    {
        if (isset($thumbnailObject->maxres)) {
            return $thumbnailObject->maxres->url;
        } elseif (isset($thumbnailObject->standard)) {
            return $thumbnailObject->standard->url;
        } elseif (isset($thumbnailObject->high)) {
            return $thumbnailObject->high->url;
        } elseif (isset($thumbnailObject->medium)) {
            return $thumbnailObject->medium->url;
        } else {
            return $thumbnailObject->default->url;
        }
    }
}
