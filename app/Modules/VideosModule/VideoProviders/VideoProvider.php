<?php

namespace App\Modules\VideosModule\VideoProviders;

use App\Interfaces\VideoInterface;
use Alaouy\Youtube\Facades\Youtube;
use App\Modules\VideosModule\VideoEntity;
use App\Exceptions\VideosCanNotBeFound;

abstract class VideoProvider
{
    protected string $country;

    public function videosByCountry(string $country): array
    {
        $this->country = $country;
        $data = $this->getVideosByCountry();
        return $this->mapToEntities($data);
    }

    abstract protected function getVideosByCountry(): array;

    abstract protected function mapToEntities(array $data): array;
}
