<?php

namespace App\Modules\VideosModule\UseCases;

use App\Interfaces\UseCaseInterface;
use App\Exceptions\VideosCanNotBeFound;
use App\Modules\VideosModule\UseCaseImplementation\VideoByCountryImplementation;

class GetLatestVideosByCountry extends VideoByCountryImplementation implements UseCaseInterface
{
    public function __construct(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return array
     * @throws VideosCanNotBeFound
     */
    public function execute(): array
    {
        return $this->fromYoutube();
    }
}
