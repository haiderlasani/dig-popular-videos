<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return [
        'title' => 'DIG country popular videos',
        'description' => 'Welcome to DIG\'s country popular videos portal,
        where you\'ll find top 5 videos for uk, nl, de, fr, es, it and gr.',
        'api' => [
            'index' => [
                'url' => url('country-hits'),
                'description' => 'Information of all countries with their popular videos. Use offset={int 1-7} parameter to setup
                result limit per page'
            ],
            'single country page' => [
                'urls' => [
                    'uk' => url('country-hits', 'uk'),
                    'de' => url('country-hits', 'de'),
                    'fr' => url('country-hits', 'fr'),
                    'es' => url('country-hits', 'es'),
                    'it' => url('country-hits', 'it'),
                    'gr' => url('country-hits', 'gr'),
                ],
                'description' => 'Information of a country with it\'s popular videos.'
            ]
        ]
    ];
});

$router->group(['namespace' => 'PopularVideos', 'prefix' => 'country-hits'], function () use ($router) {
    $router->get('/', 'CountryPopularVideosController@index');
    $router->get('{countryCode}', 'CountryPopularVideosController@show');
});


