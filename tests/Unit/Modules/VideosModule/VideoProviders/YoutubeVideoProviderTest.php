<?php

namespace Tests\Modules\VideosModule\VideoProviders;

use Tests\TestCase;
use ReflectionException;
use App\Interfaces\VideoInterface;
use App\Modules\VideosModule\VideoEntity;
use App\Modules\VideosModule\VideoProviders\YoutubeVideoProvider;

class YoutubeVideoProviderTest extends TestCase
{
    private array $youtubeData;

    /** @test */
    public function itImplementsTheVideoInterface()
    {
        $videoEntity = new VideoEntity();
        $this->assertInstanceOf(VideoInterface::class, $videoEntity);
    }

    /** @test */
    public function itGetsThumbnailWithHighestResolution()
    {
        $thumbnailData = $this->youtubeData[0]->snippet->thumbnails;

        $youtubeProvider = new YoutubeVideoProvider();
        $thumbnail = $this->invokeMethod($youtubeProvider, 'getHighestQualityThumbnail', [$thumbnailData]);
        $this->assertEquals('https//maxres', $thumbnail);

        $thumbnailData = $this->youtubeData[0]->snippet->thumbnails;
        unset($thumbnailData->maxres);

        $youtubeProvider = new YoutubeVideoProvider();
        $thumbnail = $this->invokeMethod($youtubeProvider, 'getHighestQualityThumbnail', [$thumbnailData]);
        $this->assertEquals('https//high', $thumbnail);
    }

    public function setUp(): void
    {
        parent::setUp();

        $youtube1 = new class {
            public string $id = 'code 1';
            public object $snippet;
            public object $contentDetails;

            public function __construct()
            {
                $this->contentDetails = new class {
                    public string $duration = 'duration 1';
                };

                $this->snippet = new class {
                    public string $title = 'title 1';
                    public string $description = 'description 1';
                    public string $publishedAt = 'published at 1';
                    public object $thumbnails;

                    public function __construct()
                    {
                        $this->thumbnails = new class {
                            public object $high;
                            public object $maxres;

                            public function __construct()
                            {
                                $this->high = new class {
                                    public string $url = 'https//high';
                                };

                                $this->maxres = new class {
                                    public string $url = 'https//maxres';
                                };
                            }
                        };
                    }
                };
            }
        };

        $this->youtubeData = [$youtube1];
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unset($this->youtubeData);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
