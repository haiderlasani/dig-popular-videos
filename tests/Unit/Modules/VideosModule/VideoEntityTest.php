<?php

namespace Tests\Modules\VideosModule;

use Tests\TestCase;
use App\Interfaces\VideoInterface;
use App\Modules\VideosModule\VideoEntity;

class VideoEntityTest extends TestCase
{
    /** @test */
    public function itImplementsTheVideoInterface()
    {
        $videoEntity = new VideoEntity();
        $this->assertInstanceOf(VideoInterface::class, $videoEntity);
    }

    /** @test */
    public function itMapsVideoEntityFromArray()
    {
        $title = 'A sample title';
        $description = 'A sample description';
        $duration = 'PT4M46S';
        $code = 'codeZ';
        $publishedAt = '2020-07-04  23:00:00';
        $thumbnailHQ = 'https://i.ytimg.com/vi/6aWpoIIBhkc/maxresdefault.jpg';

        $videoEntity = new VideoEntity();
        $videoEntity->setTitle($title);
        $videoEntity->setDescription($description);
        $videoEntity->setDuration($duration);
        $videoEntity->setCode($code);
        $videoEntity->setPublishedAt($publishedAt);
        $videoEntity->setThumbnailHQ($thumbnailHQ);

        $this->assertEquals($videoEntity->title(), $title);
        $this->assertEquals($videoEntity->description(), $description);
        $this->assertEquals($videoEntity->duration(), $duration);
        $this->assertEquals($videoEntity->code(), $code);
        $this->assertEquals($videoEntity->publishedAt(), $publishedAt);
        $this->assertEquals($videoEntity->thumbnailHQ(), $thumbnailHQ);
    }
}
